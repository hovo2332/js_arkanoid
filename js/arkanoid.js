const canvas = document.querySelector('.canvas');
const context = canvas.getContext('2d');
canvas.height = window.innerHeight;
canvas.width = 700;


const paddle = {
  x: (canvas.width/2) - 100,
  y: canvas.height - 10,
  dx: 4,
  width: 200,
  height: 10,
}

const ball = {
  x: canvas.width/2,
  y: paddle.y - 10,
  dx: 2,
  dy: -2,
  radius: 10,
}


const height = 40;
let blocksArr = [];
let gameStarted = false;
let rightPressed = false;
let leftPressed = false;
let interval;

const randomNumber = function (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;;
}

const randomRGB = function () {
  return `rgb(${randomNumber(250,25)}, ${randomNumber(250,100)}, ${randomNumber(250,150)})`
}

drawBlocks();
drawBall();
drawPaddle();

function drawBlocks() {
  for (let i = 0; i < randomNumber(3, 8); i++) {
    blockWidth = canvas.width / randomNumber(3, 8);
    for (let j = 0; j < canvas.width / blockWidth; j++) {

      let blockColor = randomRGB();
      context.shadowColor = 'black';
      context.shadowBlur = 4;  
      context.fillStyle = blockColor;
      context.fillRect(j * blockWidth, i * height, blockWidth, height);
      blocksObj = {
        height,
        blockWidth,
        blockColor,
        x: j * blockWidth,
        y: i * height,
      }
      blocksArr.push(blocksObj);
    }
  }
}

function drawBall() {
  context.beginPath();
  context.arc(ball.x, ball.y, ball.radius, 0, Math.PI * 2);
  context.fillStyle = "black"
  context.fill();
  context.closePath();
}

function drawPaddle() {
  context.beginPath();
  context.fillStyle = 'orange'
  context.fillRect(paddle.x, paddle.y, paddle.width, paddle.height);
  context.closePath();
}

function savedBlocks() {
  context.clearRect(0, 0, canvas.width, canvas.height);
  blocksArr.forEach(element => {
    context.fillStyle = element.blockColor;
    context.fillRect(element.x, element.y, element.blockWidth, element.height)
  });
}

document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);


function keyDownHandler(e) {
  if (e.keyCode === 39) {
    rightPressed = true;
  } else if (e.keyCode === 37) {
    leftPressed = true;
  }
  
  if(gameStarted) {
    return
  }
  if (e.keyCode === 32) {
    gameStarted = true;
    interval = setInterval(draw, 2);
  }

}

function keyUpHandler(e) {
  if (e.keyCode === 39) {
    rightPressed = false;
  } else if (e.keyCode === 37) {
    leftPressed = false;
  }
}


function detection() {
  blocksArr.forEach(cub => {
    if (ball.y <= cub.y + cub.height) {
      if (ball.x >= cub.x && ball.x <= cub.blockWidth + cub.x) {
        let index = blocksArr.indexOf(cub);

        ball.dy = -ball.dy;

        blocksArr.splice(index, 1);

        savedBlocks();

        if (blocksArr.length == 0) {
          setTimeout("alert('YOU WIN!!!')", 50);
          gameStarted = false;
          newGame();
        }
      }
    }
  });
}


function draw() {
  savedBlocks();
  drawBall();
  drawPaddle();
  detection();

  if (ball.x + ball.dx > canvas.width - ball.radius || ball.x + ball.dx < ball.radius) {
    ball.dx = -ball.dx;
  }

  if (ball.y <= 0) {
    ball.dy = -ball.dy;
  } else if (ball.y + ball.dy > canvas.height - ball.radius) {
    if (ball.x > paddle.x && ball.x < paddle.x + paddle.width) {
      ball.dy = -ball.dy;
    } else if(ball.y >= canvas.height){
      alert('GAME OVER');
      gameStarted = false;
      newGame();
    }
  }

  if (rightPressed && paddle.x < canvas.width - paddle.width) {
    paddle.x += paddle.dx;
  } else if (leftPressed && paddle.x > 0) {
    paddle.x -= paddle.dx;
  }

  ball.x += ball.dx;
  ball.y += ball.dy;
}


function newGame() {

  context.clearRect(0, 0, canvas.width, canvas.height);
  clearInterval(interval);
  
  paddle.x = 250;
  paddle.y = canvas.height - 10;
  ball.x = 350;
  ball.y = paddle.y - 10;
  ball.dx = 2;
  ball.dy = -2;
  blocksArr = [];

  drawBlocks();
  drawBall();
  drawPaddle();

}
